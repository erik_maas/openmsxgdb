#!/usr/bin/python3
# OpenMSX GDB
#
# Copyright (c) 2015-2016  Erik Maas
#    
# SPDX-License-Identifier: MIT

import unittest
import time
import re

from openmsxgdb_pack import gdbconsole

class Helper:
    def __init__(self, gdbconsole):
        self.gdbconsole = gdbconsole

    def HandleCommand(self, command, expected_response, timeout=1.0):
        if command is not None:
            self.gdbconsole.command(command)
        if expected_response is not None:
            return self.WaitForResponse(expected_response, timeout)
        return ""

    def WaitForResponse(self, expected_response, timeout=1.0):
        if expected_response is None:
            return ""
        if expected_response == "":
            return ""

        # print("*** Waiting for [{0}]".format(expected_response))
        ref_time = time.time()
        while True:
            time_left = timeout - (time.time() - ref_time)
            if time_left < 0:
                return None
            
            response = self.gdbconsole.getresponse(time_left)
            if response is not None:
                if expected_response.strip() == response.strip():
                    # print("*** found [{0}]".format(response))
                    return response
                match = re.match(expected_response, response.strip())
                if match:
                    # print("*** found [{0}]".format(response))
                    return match

            # print("*** skipping [{0}]".format(response))

    def ExecuteTestList(self, testlist):
        for test in testlist:
            print("*** Executing command [{}]".format(test["command"]))
            self.gdbconsole.command(test["command"])
            if test["response"] is not None:
                expected_responses = test["response"].split("\n")
                for expected_response in expected_responses:
                    response = self.WaitForResponse(expected_response.strip())
                    if (response is None):
                        print("*** Failed: Expected response {}, but did not receive it".format(expected_response))
                    assert(response is not None)
        self.gdbconsole.openmsx.stop(wait_for_deactive=False)


class list(unittest.TestCase):
    # @unittest.skip
    def test(self):
        g = gdbconsole.GdbConsole(program="../example/test", sourcedir=["../example"], debug=True)
        h = Helper(g)
        h.ExecuteTestList([
            { "command": "set prompt", "response": None},
            { "command": "set height 0", "response": None},
            { "command": "set width 0", "response": None},
            { "command": "set annotate 1", "response": None},
            { "command": " set verbose off", "response": None},
            { "command": "info line", "response":"No line number information available."},
            { "command": "list", "response": "\\d+\\s+void main\\(void\\)"}
        ])

class BasicTests(unittest.TestCase):
    # @unittest.skip
    def test(self):
        g = gdbconsole.GdbConsole(program="../example/test", sourcedir=["../example"], debug=True)
        h = Helper(g)
        h.ExecuteTestList([
            { "command" : "set prompt",  "response" : None},
            { "command" : "set height 0",  "response" : None},
            { "command" : "set width 0",  "response" : None},
            { "command" : "set annotate 1",  "response" : None},
            { "command" : " set verbose off",  "response" : None},
            { "command" : "info line",  "response" : "No line number information available."},
            { "command" : "list",  "response" : None},
            { "command" : "info line",  "response" : None},
            { "command" : "output 4711",  "response" : None},
            { "command" : "show language",  "response" : "The current source language is \"auto; currently c\"."},
            { "command" : "show version",  "response" : "Eriks GDB 0.0\nCopyright \\(C\\) 2016 Erik Maas"},
            { "command" : "pwd",  "response" : "Working directory /home/maase/git/openmsxgdb/tests."},
            { "command" : "info breakpoints",  "response" : "No breakpoints or watchpoints."},
            { "command" : "show history filename",  "response" : None},
            { "command" : "show history size",  "response" : "The size of the command history is 256."},
            { "command" : "set confirm off",  "response" : None},
            { "command" : "source /tmp/dddFw2Ha3",  "response" : None},
            { "command" : "info source",  "response" : "Current source file is test.c"},
            { "command" : "# reset",  "response" : None},
            { "command" : "display",  "response" : None},
            { "command" : "info display",  "response" : "There are no auto-display expressions now."},
            { "command" : "set environment TERM dumb",  "response" : None},
            { "command" : "info files",  "response" : "Symbols from \\\"../example/test\\\"."},
            { "command" : "info program",  "response" : "The program being debugged is not being run."},
            { "command" : "x /i 0x41f2",  "response" : "0x41f2 <main+5>:\tCALL 0x419e"},
            { "command" : "x /i 0x42f2",  "response" : "0x42f2 <__str_8+121>:\tSUB D"},
            { "command" : "disassemble 0x41f2,0x42f2",  "response" : "Dump of assembler code for function main:\nEnd of assembler dump."},
            { "command" : "help detach",  "response" : None},
            { "command" : "help run",  "response" : None},
            { "command" : "help step",  "response" : None},
            { "command" : "help stepi",  "response" : None},
            { "command" : "help next",  "response" : None},
            { "command" : "help nexti",  "response" : None},
            { "command" : "help until",  "response" : None},
            { "command" : "help finish",  "response" : None},
            { "command" : "help cont",  "response" : None},
            { "command" : "help signal",  "response" : None},
            { "command" : "help kill",  "response" : None},
            { "command" : "help up",  "response" : None},
            { "command" : "help down",  "response" : None},
            { "command" : "output test_othermodule",  "response" : "test_othermodule"},
            { "command" : "output test_otherfunction",  "response" : "test_otherfunction"},
            { "command" : "output function_with_assembly",  "response" : "function_with_assembly"},
            { "command" : "output launched",  "response" : "launched"},
            { "command" : "output staticBuffer",  "response" : "staticBuffer"},
            { "command" : "output has",  "response" : "has"},
        ])

class Disassemble(unittest.TestCase):
    def test(self):
        g = gdbconsole.GdbConsole(program="../example/test", sourcedir=["../example"], debug=True)
        h = Helper(g)
        h.ExecuteTestList([
            { "command" : "disassemble 0x41f2,0x42f2",  "response" : "Dump of assembler code for function main:\nEnd of assembler dump."},
        ])

class BreakpointsTestcase(unittest.TestCase):
    # @unittest.skip
    def test(self):
        g = gdbconsole.GdbConsole(program="../example/test", sourcedir=["../example"], debug=True)
        h = Helper(g)

        breakpoint_id = 0
        match = h.HandleCommand("info breakpoints", "No breakpoints or watchpoints.")
        assert (match is not None)
        match = h.HandleCommand("break test.c:57", "Breakpoint (\\d+) at 0x([0-9a-f]+): file test.c, line (\\d+).")
        assert (match is not None)
        if match:
            breakpoint_id = int(match.group(1))
        match = h.HandleCommand("info breakpoints", "Num     Type           Disp Enb Address            What")
        assert (match is not None)
        match = h.HandleCommand(None, "(\\d+)\\s+breakpoint     keep y   0x([0-9a-f]+) in main at test.c:(\\d+)")
        assert (match is not None)
        match = h.HandleCommand("delete {}".format(breakpoint_id), None)
        assert (match is not None)
        match = h.HandleCommand("info breakpoints", "No breakpoints or watchpoints.")
        assert (match is not None)

        g.openmsx.stop(wait_for_deactive=False)

if __name__ == '__main__':
    unittest.main()
