# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('license/license.txt') as f:
    license = f.read()

setup(
    name='openmsxgdb',
    version='0.0.1',
    description='openmsx GDB adapter',
    long_description=readme,
    author='Erik Maas',
    author_email='erik@maas-rijt.nl',
    url='https://bitbucket.org/erik_maas/openmsxgdb',
    license=license,
    # packages=find_packages(exclude=('tests', 'docs'))
    scripts=['bin/openmsxgdb.py'],
    packages=['openmsxgdb_pack']
)

