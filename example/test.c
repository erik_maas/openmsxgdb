/*
 * Example program to compare debugging of a program compiled using SDCC and GCC
 *
 * Copyright (c) 2015-2016  Erik Maas
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include "test_othermodule.h"

static char staticBuffer[60];

static char* localFunction(int j)
{
	static char localBuffer[40];
	int i = 2 * j;

	sprintf(&localBuffer[0], "test %d %d", j, i);

	return localBuffer;
}


int putchar(int c)
{
#ifdef __SDCC
	__asm
		ld	hl, #2+0
		add	hl, sp
		ld	a, (hl)
		call #0xa2
	__endasm;
#endif
	return c;
}

static void function_with_assembly(void)
{
	printf("Here comes the assembly\r\n");
#ifdef __SDCC
	__asm
		ld a,#0
		ld ix,#0x5f
		ld iy,(#0xfcc0)
		call #0x1c
	__endasm;
#endif
	printf("There was the assembly\r\n");
}

void main(void)
{
	int i;
	int rv;

	*((unsigned char*)0xfd29) = 0xed;	// Let the TCL script enable throttling when the code has launched

	function_with_assembly();

	sprintf(&staticBuffer[0], "Hey hallo");
	printf("staticBuffer = %s\r\n", staticBuffer);

	rv = test_otherfunction(1);
	printf("test_othermodule(1) -> %d\r\n", rv);

	for (i = 0; i < 10; i++)
	{
		char* pBuf = localFunction(i);
		printf("%s\r\n", pBuf);
	}
}
