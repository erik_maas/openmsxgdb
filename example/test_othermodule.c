

#include <stdio.h>

int test_otherfunction(int value)
{
	volatile int i;
	
	for (i = 0; i < 10; i++)
	{
		printf("tick %d\r\n", i);
	}

	return value + 100;
}
