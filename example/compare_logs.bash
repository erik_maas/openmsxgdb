#!/bin/bash
#
# OpenMSX GDB debugger, example project
#
# This filters the ddd logfiles to compare them.
#
# Copyright (c) 2015-2016  Erik Maas
#    
# SPDX-License-Identifier: MIT

cat ddd_linux.log | grep -e "^\\$.*$" -e "^<- \".*\"$" -e "^-> \".*\"$" -e "^[[:space:]]*\".*\"$" > ddd_linux.filtered.log
cat ddd_openmsx.log | grep -e "^\\$.*$" -e "^<- \".*\"$" -e "^-> \".*\"$" -e "^[[:space:]]*\".*\"$" > ddd_openmsx.filtered.log
meld ddd_linux.filtered.log ddd_openmsx.filtered.log&

