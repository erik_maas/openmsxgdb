# OpenMSX GDB debugger, example project
#
# Copyright (c) 2015-2016  Erik Maas
#    
# SPDX-License-Identifier: MIT

.DEFAULT: example

OPTIONS = -mz80 --debug --constseg DUMMY

example:
	sdasz80 -o crt0msx_rom.rel crt0msx_rom.s
	sdcc ${OPTIONS} -c test.c -o test.rel
	sdcc ${OPTIONS} -c test_othermodule.c -o test_othermodule.rel
	sdcc ${OPTIONS} --no-std-crt0 --code-loc 0x4100 --data-loc 0xc000 --out-fmt-ihx -o test.ihx crt0msx_rom.rel test_othermodule.rel test.rel 
	hex2bin -e rom -s 0x4000 -l 0x8000 test.ihx
	gcc -g test.c test_othermodule.c -o test

.PHONY: launchopenmsx
launchopenmsx: example
	# openmsx -machine "turbor" &
	openmsx -carta test.rom -script launch_fast.tcl &

.PHONY: dddmsx
dddmsx:
	ddd --trace --debugger openmsxgdb.py test 2> ddd_openmsx.log

.PHONY: ddd
ddd:
	ddd --trace test 2> ddd_linux.log

.PHONY: compare
compare:
	cat ddd_linux.log | grep -e "^\\*" -e "^<- \".*\"" -e "^-> \".*\"" -e "^[[:space:]]*\".*\"" > ddd_linux.filtered.log
	cat ddd_openmsx.log | grep -e "^\\*" -e "^<- \".*\"" -e "^-> \".*\"" -e "^[[:space:]]*\".*\"" > ddd_openmsx.filtered.log
	meld ddd_linux.log ddd_openmsx.log&

.PHONY: clean
clean:
	rm -f *.adb
	rm -f *.asm
	rm -f *.cdb
	rm -f *.ihx
	rm -f *.lib
	rm -f *.lk
	rm -f *.lst
	rm -f *.map
	rm -f *.noi
	rm -f *.rel
	rm -f *.rom
	rm -f *.sym
	rm -f main
	rm -f test
	rm -f *.log

