# README #

## What is it? ##

This program allows source level debugging of MSX programs written in C that were compiled using SDCC. It does this by interpreting generated CDB files, providing a GDB like API and using relevant openMSX debug console commands.

## What is this the goal of this? ##

* Experiment with source level debugging of C files that are compiled with SDCC.
* Being able to do source level debugging of C files using openMSX.

The idea was to implement a GDB like API and let Eclipse use this so that source level debugging from Eclipse was possible. However it seems that it is not easy to trick Eclipse into accepting this program as being GDB. DDD was relatively easy to trick.

## Status ##

Do not expect something useful yet...
Expect lots of bugs...

No effort was spent on robustness yet as things are just starting to work. At the moment the focus is on the single C-file MSX project in the example directory.

### What is working ###
* It is possible to debug ROM files without bank switching.
    * Breakpoints can be set and do work, but there is no slot checking yet.
* Single stepping in/over works for C code that is compiled. Stepping into library functions does not work properly.
* DDD can show the Z80 registers and dump some assembly.

### To do list ###
* Slot checking in breakpoints
* Showing variables
    * static
    * on stack
* Modifying variables
* Inline assembly (no clue yet what will happen)
* Mixed C/Assembly
* ROM bank switching support
    * Likely requires some project file to describe the composition of the ROM. (Multiple CDB files, data pages, assembly)
* Unit-tests...

## Usage ##

To debug the example code using DDD

* `make`
    * Builds the example code
* `make launchopenmsx`
    * Launches openmsx with the example ROM in slot-1
* `make dddmsx`
    * Launches DDD with openmsxgdb

## Requirements ##

Versions mentioned are the versions used during development. Lower/higher versions might just work.

* openMSX (Version 0.11.0)
* DDD (Version 3.3.12)
* SDCC version 3 (Version 3.4)
* [hex2bin](https://sourceforge.net/projects/hex2bin/?source=navbar) (Version 2.1)
* Python 2.7

## References ##

* [CDB file format](http://sdcc.sourceforge.net/mediawiki/index.php/CDB_File_Format)
* [Controlling openMSX from External Applications](http://openmsx.org/manual/openmsx-control.html)
* [Debugging with GDB](https://sourceware.org/gdb/current/onlinedocs/gdb/)

## License ##

(c) Copyright 2015-2016 Erik Maas <erik@maas-rijt.nl>

SPDX-License-Identifier: MIT
For the full license text, see license.txt located in the license directory.